#!/usr/bin/env bash
set -e

VERSION=$1
if [ -z "$VERSION" ]; then
    echo "Usage: $0 <version>"
    echo "Example: $0 3.12.4-slim"
    exit 1
fi

docker pull python:$VERSION
docker tag python:$VERSION registry.gitlab.com/octobrowser-public/registry/python:$VERSION
docker push registry.gitlab.com/octobrowser-public/registry/python:$VERSION

echo 'Done!'
